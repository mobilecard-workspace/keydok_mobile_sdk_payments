package com.keydok.mobile.payments.common.repository

import com.keydok.mobile.payments.common.models.PaymentData
import com.keydok.mobile.payments.common.models.PaymentResponse
import com.keydok.mobile.payments.common.repository.api.PaymentApi
import com.noheltcj.rxcommon.observables.Single

class PaymentRepositoryImpl:PaymentRepository {
    override fun checkout(
        paymentApi: PaymentApi,
        onNext: (it: PaymentResponse?) -> Unit,
        onError: (it: Throwable) -> Unit
    ):Single<PaymentResponse> {
        return paymentApi.checkout()
    }
}