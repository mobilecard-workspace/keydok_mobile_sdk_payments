package com.keydok.mobile.payments.common.repository.api

import com.keydok.mobile.payments.common.models.PaymentData
import com.keydok.mobile.payments.common.models.PaymentResponse
import com.noheltcj.rxcommon.observables.Single


interface PaymentApi {
    fun checkout(): Single<PaymentResponse>
}