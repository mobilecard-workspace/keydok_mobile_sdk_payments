package com.keydok.mobile.payments.common.repository.api

import com.keydok.mobile.payments.common.ApplicationDispatcher
import com.keydok.mobile.payments.common.models.PaymentResponse
import com.keydok.mobile.payments.common.models.PaymentResponseImpl
import com.noheltcj.rxcommon.observables.Single
import com.noheltcj.rxcommon.subjects.PublishSubject
import io.ktor.client.HttpClient
import io.ktor.client.request.get
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.serialization.json.Json

class PaymentApiImpl:PaymentApi {

    private var client = HttpClient()

    override fun checkout(): Single<PaymentResponse> {

        val subject = PublishSubject<PaymentResponse>()
        GlobalScope.apply {
            launch(ApplicationDispatcher) {
                var api:String = client.get("https://0yi3dzwh66.execute-api.us-east-1.amazonaws.com/develop/pruebas")

                print(api)

                var response = """
                    {
                        "code": "200",
                        "message": ""
                        "data": "**** html ****"
                    }
                """.trimIndent()

                var objResponse = Json.parse(PaymentResponseImpl.serializer(),response)

                subject.onNext(objResponse)
            }
        }

        return subject.toSingle()
    }
}