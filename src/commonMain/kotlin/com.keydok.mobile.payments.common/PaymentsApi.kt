package com.keydok.mobile.payments.common

import com.keydok.mobile.payments.common.models.PaymentResponse
import com.keydok.mobile.payments.common.repository.PaymentRepositoryImpl
import com.keydok.mobile.payments.common.repository.api.PaymentApiImpl
import com.noheltcj.rxcommon.observables.Single
import com.noheltcj.rxcommon.observers.NextTerminalObserver
import com.noheltcj.rxcommon.subjects.PublishSubject

object PaymentsApi {

    private val repository = PaymentRepositoryImpl()

    fun sendPaymentData(str:String):String {
        repository.checkout(
            PaymentApiImpl(),
            {
                println("#### --- ")
                println(it.toString())
                println("#### --- ")
            },
            {
                println("#### AppConfigViewModel loadAppConfig ####")
            }
        )
        //subject.onNext(dummyHtml)
        return ""
    }

    fun checkout(str:String):Single<PaymentResponse>{
        return Single<PaymentResponse>(createWithEmitter = { se ->
            repository.checkout(
                PaymentApiImpl(),
                {
                    println("#### --- ")
                    println(it.toString())
                    println("#### --- ")
                },
                {
                    println("#### error ####")
                }
            ).subscribe(
                NextTerminalObserver({
                    se.next(it)
                }, {
                    se.terminate(it)
                })
            )
        })
    }
}