package com.keydok.mobile.payments.common.models

import kotlinx.serialization.Serializable

/**
 * @suppress
 */
@Serializable
data class PaymentResponseImpl(
    override var code: String = "",
    override var message: String = "",
    override var data: String = ""
) :PaymentResponse