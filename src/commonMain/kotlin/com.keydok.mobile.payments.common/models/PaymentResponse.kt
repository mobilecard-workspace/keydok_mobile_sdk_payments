package com.keydok.mobile.payments.common.models

/**
 * @suppress
 */
interface PaymentResponse {
    var code:String
    var message:String
    var data:String
}